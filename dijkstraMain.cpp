#define OLC_PGE_APPLICATION
#include <cstdint>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include "olcPixelGameEngine.h"
const int INF = 2147483647;

//Esitell��n luokka Node, solmu
class Node {
public:
	//Sijainti ruudun X,Y -koordinaatistossa
	olc::vf2d screenPosition;
	//Solmun nmi
	std::string name;
	//Solmun paino, k�ytet��n laskiessa.
	float weight = INF;
	//Mit� kauttaa solmuun p��dyttiin
	Node* viaNode;
	//Onko solmu tunnettu
	bool known = false;
	//Mitk� on t�m�n solmu viereiset solmut (= mihin t�st� solmusta voi matkustaa)
	std::vector<Node*> adjacents;
	Node();
	Node(std::string name,float x, float y);
	bool operator== (const Node n1);
};

//Esitell��n luokka Graph, graafi
class Graph {
public:
	//Some structure of nodes
	std::vector<Node> nodes;
	Graph();
};
bool nodeWeigthComparator(Node* n1,Node* n2) {
	if(n1!=n2)
	return n1->weight < n2->weight;
	return false;
}
//Itse algoritmin toteutus on t�ss�, parametreina kaikki graafin solmut, alkusolmu ja loppusolmu
std::vector<Node*> findShortestPath(std::vector<Node> *nodes,Node* begin, Node* end) {
	//Asetetaan aloitussolmun paino nollaksi:
	begin->weight = 0.0f;
	begin->known = true;
	//Jos alku on sama kuin loppu, palautetaan 'reitti' samantien
	if (begin == end) {
		end->weight = 0.0f;
		std::vector<Node*> retVector;
		retVector.push_back(begin);
		retVector.push_back(end);
		return retVector;
	}

	//K�sitelt�v� solmu = aloitussolmu
	int i = 0;
	Node* currentNode = begin;
	do {
		//Jos naapurisolmu ei ole tunnettu JA sen paino on suurempi kuin uusi paino
		//Asetetaan k�sitelt�v�n solmun naapureiden painot vektorin magnitudiksi
		//
		//naapurin_paino = (k�sitelt�v�n_solmun_vektori - naapurin_vektori).magnitudi() + oma_paino
		for (int i = 0; i < currentNode->adjacents.size(); i++) {
			float newWeigth = (currentNode->screenPosition - currentNode->adjacents[i]->screenPosition).mag() + currentNode->weight;
			if (!currentNode->adjacents[i]->known && currentNode->adjacents[i]->weight > newWeigth) {
				currentNode->adjacents[i]->weight = newWeigth;
				currentNode->adjacents[i]->viaNode = currentNode;
			}
		}
		//Kaikki naapurit k�sitelty -> solmu tunnetuksi
		currentNode->known = true;
		//Valitaan seuraava solmu
		for (int i = 0; i < nodes->size(); i++) {
			if(currentNode->known && !nodes->at(i).known)
				currentNode = &nodes->at(i);
			if (nodes->at(i).weight <currentNode->weight && !nodes->at(i).known) {
				currentNode = &nodes->at(i);
			}
		}
		//Jos tuntemattomien solmujen pienin solmu on sama kuin lopetussolmu, lopetetaan homma
		i++;
		//Jos nykyisen solmun paino on '��ret�n', ei yhteytt� lopetussolmuun ole, lopetetaan
		if (currentNode->weight == INF) {
			std::vector<Node*> empty;
			return empty;
		}
	} while (currentNode != end);
	std::cout << "Total nodes checked:"<< i << std::endl;;



	//Palautettava vektori
	std::vector<Node*> retVector;
	//Aloitetaan lopusta
	currentNode = end;
	//Ty�nnet��n lopetussolmu vektoriin
	retVector.push_back(currentNode);
	while (currentNode != begin) {
		//Valitaan se solmu, jonka kautta nykyiseen solmuun tultiin
		currentNode = currentNode->viaNode;
		//Laitetaan vektoriin
		retVector.push_back(currentNode);
	}
	//Py�r�ytet��n ymp�ri
	std::reverse(retVector.begin(), retVector.end());
	//Palautetaan valmis reitti
	return retVector;
}
//Luokka DijkstraDemo perii PixelGameEnginen esityksen visualisointia varten
class DijkstraDemo : public olc::PixelGameEngine
{
private:
	Graph graph;
	Node* begin = nullptr;
	Node* end = nullptr;
	std::vector<Node*> path;
	float frametime = 33.33;
	bool done = false;
	bool noPath = false;
public:
	DijkstraDemo()
	{
		sAppName = "Dijkstra Demonstration";
	}
	bool OnUserCreate() override
	{
		srand(time(NULL));
		graph = Graph();
		begin = nullptr;
		end = nullptr;
		path.clear();
		frametime = 33.33;
		done = false;
		noPath = false;
		return true;
	}
	bool OnUserUpdate(float fElapsedTime) override
	{
		//Millisecond time for framerate limiter
		uint64_t time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		Clear(olc::WHITE);
		for (int i = 0; i < graph.nodes.size(); i++) {
			//Piirret��n polut
			for (Node* adj : graph.nodes[i].adjacents) {
				DrawLine(graph.nodes[i].screenPosition, adj->screenPosition, olc::BLACK);
			}
			//Piirret��n solmut
			if (begin == &graph.nodes[i] || end == &graph.nodes[i]) {
				FillCircle(graph.nodes[i].screenPosition.x, graph.nodes[i].screenPosition.y , 4, olc::GREEN);
			}
			else if ((GetMouseX() > graph.nodes[i].screenPosition.x - 4 && GetMouseX() < graph.nodes[i].screenPosition.x + 4) && (GetMouseY() > graph.nodes[i].screenPosition.y - 4 && GetMouseY() < graph.nodes[i].screenPosition.y + 4)) {
				FillCircle(graph.nodes[i].screenPosition.x , graph.nodes[i].screenPosition.y , 4, olc::YELLOW);
			}
			else {
				FillCircle(graph.nodes[i].screenPosition.x , graph.nodes[i].screenPosition.y , 4, olc::BLACK);
			}
		}
		if(begin==nullptr)
		DrawString(0, 0, "Select start node", olc::BLACK,2);
		else if (end == nullptr)
		{
			DrawString(0, 0, "Select end node", olc::BLACK, 2);
		}
		else if(!done)
		{
				DrawString(0, 0, "Calculating...", olc::BLACK, 2);
				path = findShortestPath(&graph.nodes, begin, end);
				done = true;
				if (path.size() == 0)
					noPath = true;
		}
		if (done) {
			if (noPath) {
				DrawString(0, 0, "No path between start and end node", olc::BLACK, 2);
			}
			else {
				for (int i = 0; i < path.size(); i++) {
					//Piirret��n polut
					for (int j = 0; j < path.size() - 1; j++) {
						DrawLine(path.at(j)->screenPosition, path.at(j + 1)->screenPosition, olc::RED);
					}
					//Piirret��n solmut
					FillCircle(path.at(i)->screenPosition.x, path.at(i)->screenPosition.y, 4, olc::RED);
				}
				DrawString(0, 0, "Distance: " + std::to_string(end->weight), olc::BLACK, 2);
				DrawString(0, 20, "Click to try again", olc::BLACK, 2);
			}
		}
		if (IsFocused()) {
			if (GetMouse(0).bPressed) {
				if (done)
					OnUserCreate();
				for (int i = 0; i < graph.nodes.size(); i++) {
					if ((GetMouseX() > graph.nodes[i].screenPosition.x - 5 && GetMouseX() < graph.nodes[i].screenPosition.x + 5) && (GetMouseY() > graph.nodes[i].screenPosition.y - 5 && GetMouseY() < graph.nodes[i].screenPosition.y + 5))
						if (begin == nullptr)
							begin = &graph.nodes[i];
						else if (end == nullptr)
							end = &graph.nodes[i];
				}
			}
		}
		//Wait until fratmetime >= 33ms
		while(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - time < frametime){}
		return true;
	}
};
int main()
{
	DijkstraDemo demo;
	if (demo.Construct(1024, 768, 1, 1))
		demo.Start();

	return 0;
}

Graph::Graph() {
	//Luodaan solmut
	for (int i = 0; i < 600; i++) {
		Node n = Node(std::to_string(i), rand() % 1020 + 1, rand() % 720 + 40);
		nodes.push_back(n);
	}
	//Luodaan solmujen naapurit (jos et�isyys alle 200, lasketaan naapuriksi)
	for (int i = 0; i < nodes.size();i++) {
		for (int j = 0; j < nodes.size(); j++) {
			//En jaksanut kirjoittaa != operaattoria
			if( (nodes[j] == nodes[i]) == false && (nodes[j].screenPosition - nodes[i].screenPosition).mag() < 60.0f)
				nodes[i].adjacents.push_back(&nodes[j]);
		}
	}
}

Node::Node() {
	viaNode = nullptr;
	name = "";
}
Node::Node(std::string name, float x, float y) {
	this->name = name;
	screenPosition = { x,y };
}
bool Node :: operator== (const Node n) {
	if (this->name == n.name && this->screenPosition.x == n.screenPosition.x && this->screenPosition.y == n.screenPosition.y)
		return true;
	return false;
}




